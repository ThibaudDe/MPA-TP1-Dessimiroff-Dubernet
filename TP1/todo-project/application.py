#! /usr/bin/env python3
from db import DB
from flask import Flask, render_template
app = Flask(__name__)
app.debug = True

@app.route('/')
def home():
    return render_template(
        "home.html")

#todo={None : ["rien", "pas grand chose", "sieste"], "fred" : ["se lever", "rien"], "robert" : ["sieste", "cours"]}

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    db = DB()
    res = db.get(name)
    l = []
    for elem in res:
        l.append(elem[1])
    return render_template(
        "user.html",
        name=name,
        todo=l)

@app.route('/users/')
def users():
    db = DB()
    resInit = db.users()
    res={}
    for name, tache in resInit:
        if name not in res.keys():
            res[name]=[tache]
        else:
            res[name].append(tache)
    return render_template(
        "users.html",
        data = res
        )

if __name__ == '__main__':
    app.run(host="0.0.0.0")
